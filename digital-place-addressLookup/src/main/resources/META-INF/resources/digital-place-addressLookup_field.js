AUI.add(
	'digital-place-addressLookup-form-field',
	function(A) {
		
		var CSS_ACTIVE = A.getClassName('active');

		var CSS_DROP_CHOSEN = A.getClassName('drop', 'chosen');

		var CSS_FORM_FIELD_CONTAINER = A.getClassName('lfr', 'ddm', 'form', 'field', 'container');
		
		var CSS_HIDE = A.getClassName('hide');

		var CSS_SELECT_ARROW_DOWN = A.getClassName('select', 'arrow', 'down', 'container');

		var CSS_SELECT_OPTION_ITEM = A.getClassName('select', 'option', 'item');

		var CSS_SELECT_TRIGGER_ACTION = A.getClassName('select', 'field', 'trigger');

		var AddressLookupField = A.Component.create(
			{
				ATTRS: {
					options: {
						getter: '_getOptions',
						state: true,
						validator: Array.isArray,
						value: []
					},

					type: {
						value: 'digital-place-addressLookup-form-field'
					},
					
					triggers: {
						value: []
					},
					
					value: {
						state: true,
						value: []
					}
				},

				EXTENDS: Liferay.DDM.Renderer.Field,

				NAME: 'digital-place-addressLookup-form-field',

				prototype: {
					initializer: function() {
						var instance = this;

						instance._open = false;
						instance._addressVisible = false;
						instance._postCodeValue = "";
						
						//instance._findPostCodeButton = A.one('.find-postcode-trigger');
						//instance._findPostCodeButton.on('click', instance._handleFindPostcodeClick, instance);

						//console.info(instance._findPostCodeButton);

						instance._eventHandlers.push(
							A.one('doc').after('click', A.bind(instance._afterClickOutside, instance)),
							instance.bindContainerEvent('click', instance._handleFindPostcodeClick, '.find-postcode-trigger'),
							instance.bindContainerEvent('click', instance._handleContainerClick, '.' + CSS_FORM_FIELD_CONTAINER)
						);
					},
					
					_afterClickOutside: function(event) {
						var instance = this;

						if (!instance._preventDocumentClick && instance._isClickingOutside(event)) {
							instance.closeList();
						}

						instance._preventDocumentClick = false;
					},

					cleanSelect: function() {
						var instance = this;

						var inputNode = instance.getInputNode();

						inputNode.setAttribute('selected', false);

						instance.set('value', []);
					},

					closeList: function() {
						var instance = this;

						if (instance._isListOpen()) {
							var container = instance.get('container');

							container.one('.' + CSS_DROP_CHOSEN).addClass(CSS_HIDE);

							container.one('.' + CSS_SELECT_TRIGGER_ACTION).removeClass(CSS_ACTIVE);

							instance._open = false;

							instance.fire('closeList');
						}
					},
					
					findPostcode: function(event) {
						var instance = this;

						var container = instance.get('container');

						instance.cleanSelect();
						
						instance.retrieveAddresses();

						container.one('.input-select-wrapper').removeClass(CSS_HIDE);
						
						instance._addressVisible = true;
					},
					
					focus: function() {
						var instance = this;

						var container = instance.get('container');

						var arrowSelect = container.one('.' + CSS_SELECT_ARROW_DOWN);

						arrowSelect.focus();
					},
					
					_getOptions: function(options) {
						return options || [];
					},
					
					_getSelectTriggerAction: function() {
						var instance = this;

						return instance.get('container').one('.' + CSS_SELECT_TRIGGER_ACTION);
					},
					
					getTemplateContext: function() {
						var instance = this;

						return A.merge(
							AddressLookupField.superclass.getTemplateContext.apply(instance, arguments),
							{
								open: instance._open,
								addressVisible: instance._addressVisible,
								options: instance.get('options'),
								value: instance.getValue(),
								postCodeValue: instance._postCodeValue
							}
						);
					},
					
					getValue: function() {
						var instance = this;

						return instance.get('value') || [];
					},

					_handleContainerClick: function(event) {
						var instance = this;

						var target = event.target;

						var optionNode = target.ancestor('.' + CSS_SELECT_OPTION_ITEM, true);

						var selectTrigger = target.ancestor('.' + CSS_SELECT_TRIGGER_ACTION, true);
						
						if (optionNode) {
							instance._handleItemClick(optionNode);
						}
						else if (selectTrigger){
							instance._handleSelectTriggerClick(event);
						}
						
						instance._preventDocumentClick = true;
					},

					
					_handleFindPostcodeClick: function(event) {
						var instance = this;
						
						instance.findPostcode(event);
						
						instance._preventDocumentClick = true;
						
					},
					
					_handleItemClick: function(target) {
						var instance = this;

						var value = instance.get('value') || [];

						var currentTarget = target;

						var itemValue = currentTarget.getAttribute('data-option-value');

						if (itemValue === '') {
							value = [];
						}
						else {
							value = [itemValue];
						}

						instance._open = false;

						instance.setValue(value);

						instance.focus();

						instance._fireStartedFillingEvent();
					},

					
					_handleSelectTriggerClick: function(event) {
						var instance = this;

						var target = event.target;

						instance.toggleList(event);
					},
					
					_isClickingOutside: function(event) {
						var instance = this;

						var container = instance.get('container');

						var triggers = instance.get('triggers');

						if (triggers.length) {
							for (var i = 0; i < triggers.length; i++) {
								if (triggers[i].contains(event.target)) {
									return false;
								}
							}
						}

						return !container.contains(event.target);
					},

					_isListOpen: function() {
						var instance = this;

						var container = instance.get('container');

						var dropChosen = container.one('.' + CSS_DROP_CHOSEN);

						if (dropChosen) {
							var openList = dropChosen.hasClass(CSS_HIDE);

							return !openList;
						}

						return false;
					},

					openList: function() {
						var instance = this;

						instance._getSelectTriggerAction().addClass(CSS_ACTIVE);

						instance.get('container').one('.' + CSS_DROP_CHOSEN).removeClass(CSS_HIDE);

						instance._open = true;
					},

					render: function() {
						var instance = this;						
						
						AddressLookupField.superclass.render.apply(instance, arguments);
						
						return A.merge(
								AddressLookupField.superclass.render.apply(instance, arguments),
								{
									open: instance._open,
									addressVisible: instance._addressVisible,
									options: instance.get('options'),
									value: instance.getValue(),
									postCodeValue: instance._postCodeValue
								}
						);

						return instance;
					},

					retrieveAddresses: function(event) {
						var instance = this;

						var container = instance.get('container');

						var postCodeInput = container.one('#postcodeInput');
						
						var addressSelect = container.one('.dropdown-menu .inline-scroller');
						
						var postCodeServiceUrl = 'http://localhost:8080/o/web-services/property/find?postcode=' + postCodeInput.val();

						console.info("retrieveAddresses");
						console.info("postCodefield: " + postCodeInput);
						console.info("postCodeValue: " + postCodeInput.val());

						A.io.request(postCodeServiceUrl, {
				             dataType: 'json',
				             method: 'GET',
				             on: {
					             success: function() {
					            	 var data=this.get('responseData');
					                 var addressOptions = new Array();
									
					                 var addressesData = data.response.addresses;
					                 	
					                 for (i = 0; i < addressesData.length; i++) {
										  var address = addressesData[i];
										  
										  var newOption = new Object();
										  newOption.label = address.addressFull;
										  newOption.value = address.addressFull;
										  
										  addressOptions.push(newOption);										  					  
					                 }
									
					                 instance._postCodeValue = postCodeInput.val();
									
					                 instance.set('options', addressOptions);
				                 
					                 instance.render();
					             }
				             }
						});
					},
					
					setValue: function(value) {
						var instance = this;

						instance.set('value', value);
						
						instance.render();
					},

					toggleList: function(event) {
						var instance = this;

						if (instance._isListOpen()) {
							instance.closeList();
						}
						else {
							instance.openList();
						}
					}
				}
			}
		);

		Liferay.namespace('DDM.Field').AddressLookup = AddressLookupField;
	},
	'',
	{
		requires: ['liferay-ddm-form-renderer-field']
	}
);