import Component from 'metal-component';
import Soy from 'metal-soy';
import {Align} from 'metal-position';

import templates from './digital-place-addressLookup.soy';


let AddressLookupTemplates = [];

if (!window.DDMAddressLookup) {
	window.DDMAddressLookup = {

	};
}

for (let template in templates) {
	if (template !== 'templates') {
		class C extends Component {};
		Soy.register(C, templates, template);
		C.Soy = Soy;
		AddressLookupTemplates.push(
			{
				key: template,
				component: C
			}
		);
		window.DDMAddressLookup[template] = C;
	}
}

window.DDMAddressLookup.Align = Align;

export default AddressLookupTemplates;