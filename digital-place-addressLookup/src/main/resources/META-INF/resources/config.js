;(function() {
	AUI().applyConfig(
		{
			groups: {
				'field-digital-place-addressLookup': {
					base: MODULE_PATH + '/',
					combine: Liferay.AUI.getCombine(),
					filter: Liferay.AUI.getFilterConfig(),
					modules: {
						'digital-place-addressLookup-form-field': {
							condition: {
								trigger: 'liferay-ddm-form-renderer'
							},
							path: 'digital-place-addressLookup_field.js',
							requires: [
								'liferay-ddm-form-renderer-field'
							]
						}
					},
					root: MODULE_PATH + '/'
				}
			}
		}
	);
})();