package com.liferay.docs.formfieldtype.form.field;

import com.liferay.dynamic.data.mapping.form.field.type.BaseDDMFormFieldType;
import com.liferay.dynamic.data.mapping.form.field.type.DDMFormFieldType;

import org.osgi.service.component.annotations.Component;

/**
 * @author ruben.lopezseoane
 */
@Component(
	immediate = true,
	property = {
		"ddm.form.field.type.description=digital-place-addressLookup-description",
		"ddm.form.field.type.display.order:Integer=10",
		"ddm.form.field.type.icon=text",
		"ddm.form.field.type.js.class.name=Liferay.DDM.Field.AddressLookup",
		"ddm.form.field.type.js.module=digital-place-addressLookup-form-field",
		"ddm.form.field.type.label=digital-place-addressLookup-label",
		"ddm.form.field.type.name=digitalPlaceAddressLookup"
	},
	service = DDMFormFieldType.class
)
public class AddressLookupDDMFormFieldType extends BaseDDMFormFieldType {

	@Override
	public String getName() {
		return "digitalPlaceAddressLookup";
	}

}