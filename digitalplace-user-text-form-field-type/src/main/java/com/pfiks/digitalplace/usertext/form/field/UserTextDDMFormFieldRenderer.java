package com.pfiks.digitalplace.usertext.form.field;

import java.util.Map;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.form.field.type.BaseDDMFormFieldRenderer;
import com.liferay.dynamic.data.mapping.form.field.type.DDMFormFieldRenderer;
import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.render.DDMFormFieldRenderingContext;
import com.liferay.portal.kernel.template.Template;
import com.liferay.portal.kernel.template.TemplateConstants;
import com.liferay.portal.kernel.template.TemplateResource;

/**
 * @author ruben.lopezseoane
 */
@Component(
	immediate = true,
	property = "ddm.form.field.type.name=digitalplaceUserTextFormFieldType",
	service = DDMFormFieldRenderer.class
)
public class UserTextDDMFormFieldRenderer extends BaseDDMFormFieldRenderer {

	private UserTextDDMFormFieldTemplateContextContributor userTextDDMFormFieldTemplateContextContributor;
	
	private TemplateResource _templateResource;
	
	@Override
	public String getTemplateLanguage() {
		return TemplateConstants.LANG_TYPE_SOY;
	}

	@Override
	public String getTemplateNamespace() {
		return "DDMUserText.render";
	}

	@Override
	public TemplateResource getTemplateResource() {
		return _templateResource;
	}

	@Activate
	protected void activate(Map<String, Object> properties) {
		_templateResource = getTemplateResource(
			"/META-INF/resources/digitalplace-user-text-form-field-type.soy");
	}
	
	@Override
	protected void populateOptionalContext(
	    Template template, DDMFormField ddmFormField,
	    DDMFormFieldRenderingContext ddmFormFieldRenderingContext) {

	    Map<String, Object> parameters =
	    		userTextDDMFormFieldTemplateContextContributor.getParameters(
	         ddmFormField, ddmFormFieldRenderingContext);

	    template.putAll(parameters);
	}

	@Reference
	public void setUserTextDDMFormFieldTemplateContextContributor(
			UserTextDDMFormFieldTemplateContextContributor userTextDDMFormFieldTemplateContextContributor) {
		this.userTextDDMFormFieldTemplateContextContributor = userTextDDMFormFieldTemplateContextContributor;
	}
	
	
}