package com.pfiks.digitalplace.usertext.form.field;

import org.osgi.service.component.annotations.Component;

import com.liferay.dynamic.data.mapping.form.field.type.BaseDDMFormFieldType;
import com.liferay.dynamic.data.mapping.form.field.type.DDMFormFieldType;
import com.liferay.dynamic.data.mapping.form.field.type.DDMFormFieldTypeSettings;

/**
 * @author ruben.lopezseoane
 */
@Component(
	immediate = true,
	property = {
		"ddm.form.field.type.description=digitalplace-user-text-form-field-type-description",
		"ddm.form.field.type.display.order:Integer=10",
		"ddm.form.field.type.icon=text",
		"ddm.form.field.type.js.class.name=Liferay.DDM.Field.UserText",
		"ddm.form.field.type.js.module=digitalplace-user-text-form-field-type-form-field",
		"ddm.form.field.type.label=digitalplace-user-text-form-field-type-label",
		"ddm.form.field.type.name=digitalplaceUserTextFormFieldType"
	},
	service = DDMFormFieldType.class
)
public class UserTextDDMFormFieldType extends BaseDDMFormFieldType {

	@Override
	public String getName() {
		return "digitalplaceUserTextFormFieldType";
	}
	
	@Override
	public Class<? extends DDMFormFieldTypeSettings>
	    getDDMFormFieldTypeSettings() {

	    return UserTextDDMFormFieldTypeSettings.class;
	}


}