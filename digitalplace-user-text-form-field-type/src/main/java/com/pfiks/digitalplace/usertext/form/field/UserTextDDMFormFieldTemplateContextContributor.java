package com.pfiks.digitalplace.usertext.form.field;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.form.field.type.DDMFormFieldTemplateContextContributor;
import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.render.DDMFormFieldRenderingContext;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.security.auth.PrincipalThreadLocal;
import com.liferay.portal.kernel.service.UserLocalService;

@Component(
	    immediate = true,
	    property = "ddm.form.field.type.name=digitalplaceUserTextFormFieldType",
	    service = {
	        DDMFormFieldTemplateContextContributor.class,
	        UserTextDDMFormFieldTemplateContextContributor.class
	    }
	)
public class UserTextDDMFormFieldTemplateContextContributor implements DDMFormFieldTemplateContextContributor{

	public static final String USER_FIELD_NAME_PARAMETER = "userFieldName";
	
	public static final String EXPANDO_FIELD_PARAMETER = "expandoField";
	
	private UserLocalService useLocalService;
	
	private static final Log LOG = LogFactoryUtil.getLog(UserTextDDMFormFieldTemplateContextContributor.class);
	
    @Override
    public Map<String, Object> getParameters(
        DDMFormField ddmFormField,
        DDMFormFieldRenderingContext ddmFormFieldRenderingContext) {

        Map<String, Object> parameters = new HashMap<>();

        String userFieldName = (String)ddmFormField.getProperty(USER_FIELD_NAME_PARAMETER);
        boolean isExpandoField = (Boolean)ddmFormField.getProperty(EXPANDO_FIELD_PARAMETER);
        
        String predefinedUserFieldValue = getUserFieldValue(userFieldName, isExpandoField);
		
        if (predefinedUserFieldValue != null) {
        	parameters.put("predefinedValue", predefinedUserFieldValue);
        }
		
        return parameters;
    }
    
    private String getUserFieldValue(String userFieldName, boolean isExpandoField) {
    	
    	if (userFieldName == null) {
    		return null;
    	}
    	
    	try {
	    	User user = useLocalService.getUser(PrincipalThreadLocal.getUserId());
			
	    	if (isExpandoField) {
	    		Serializable value = user.getExpandoBridge().getAttribute(userFieldName);
	    		
	    		if (value != null) {
	    			return value.toString();
	    		}
	    	}
	    	else {
	    		String userFieldMethod = "get" + userFieldName.substring(0,1).toUpperCase() + userFieldName.substring(1);
	    			
	    		Method userFieldGetMethod = User.class.getMethod(userFieldMethod);
	    			
	    		if (userFieldGetMethod != null) {
	    			Object objectReturned = userFieldGetMethod.invoke(user);
	    			return (objectReturned == null ? null : objectReturned.toString());
	    		}
	    	}
    	}
    	catch (Exception e) {
    		LOG.error(e);
    	}
    	
    	return null;
    }
    
	@Reference
	public void setUseLocalService(UserLocalService useLocalService) {
		this.useLocalService = useLocalService;
	}
	
	
}
