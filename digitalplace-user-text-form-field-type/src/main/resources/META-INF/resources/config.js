;(function() {
	AUI().applyConfig(
		{
			groups: {
				'field-digitalplace-user-text-form-field-type': {
					base: MODULE_PATH + '/',
					combine: Liferay.AUI.getCombine(),
					filter: Liferay.AUI.getFilterConfig(),
					modules: {
						'digitalplace-user-text-form-field-type-form-field': {
							condition: {
								trigger: 'liferay-ddm-form-renderer'
							},
							path: 'digitalplace-user-text-form-field-type_field.js',
							requires: [
								'liferay-ddm-form-renderer-field'
							]
						}
					},
					root: MODULE_PATH + '/'
				}
			}
		}
	);
})();