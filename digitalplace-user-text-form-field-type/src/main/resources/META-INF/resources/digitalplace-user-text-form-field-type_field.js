AUI.add(
	'digitalplace-user-text-form-field-type-form-field',
	function(A) {
		var UserTextField = A.Component.create(
			{
				ATTRS: {
					type: {
						value: 'digitalplace-user-text-form-field-type-form-field'
					}
				},

				EXTENDS: Liferay.DDM.Renderer.Field,

				NAME: 'digitalplace-user-text-form-field-type-form-field',

				prototype: {
				}
			}
		);

		Liferay.namespace('DDM.Field').UserText = UserTextField;
	},
	'',
	{
		requires: ['liferay-ddm-form-renderer-field']
	}
);