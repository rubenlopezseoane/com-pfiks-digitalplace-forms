import Component from 'metal-component';
import Soy from 'metal-soy';

import templates from './digitalplace-user-text-form-field-type.soy';

/**
 * UserText Component
 */
class UserText extends Component {}

// Register component
Soy.register(UserText, templates, 'render');

if (!window.DDMUserText) {
	window.DDMUserText = {

	};
}

window.DDMUserText.render = UserText;

export default UserText;