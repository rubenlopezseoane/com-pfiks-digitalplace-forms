package com.pfiks.digitalplace.usertext.form.field;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.doReturn;
import static org.powermock.api.mockito.PowerMockito.spy;
import static org.powermock.api.mockito.PowerMockito.verifyPrivate;
import static org.powermock.api.mockito.PowerMockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.render.DDMFormFieldRenderingContext;
import com.liferay.portal.kernel.template.Template;
import com.liferay.portal.kernel.template.TemplateConstants;
import com.liferay.portal.kernel.template.TemplateResource;

@RunWith(MockitoJUnitRunner.class)
public class UserTextDDMFormFieldRendererTest {

	private UserTextDDMFormFieldRenderer userTextDDMFormFieldRenderer;

	@Mock
	private DDMFormField mockDDMFormField;
	
	@Mock
	private Template mockTemplate;
	
	@Mock
	private DDMFormFieldRenderingContext mockDDMFormFieldRenderingContext;
	
	@Mock
	private TemplateResource mockTemplateResource;
	
	@Mock
	private UserTextDDMFormFieldTemplateContextContributor mockUserTextDDMFormFieldTemplateContextContributor;
	
	@Before
	public void setUp() {
		userTextDDMFormFieldRenderer = new UserTextDDMFormFieldRenderer();
		userTextDDMFormFieldRenderer.setUserTextDDMFormFieldTemplateContextContributor(mockUserTextDDMFormFieldTemplateContextContributor);
	}
	
	@Test
	public void getTemplateLanguage_whenNoErrors_returnsSoyTemplate() {
		String language = userTextDDMFormFieldRenderer.getTemplateLanguage();
		assertThat(language, equalTo("soy"));
	}
	
	@Test
	public void getTemplateNamespace_whenNoErrors_returnsAddressFormFieldNamespace() {
		String namespace = userTextDDMFormFieldRenderer.getTemplateNamespace();
		assertThat(namespace, equalTo("DDMUserText.render"));
	}

	@Test
	public void getTemplateLanguage_whenNoErrors_returnsSoyTemplateLanguage() {
		String language = userTextDDMFormFieldRenderer.getTemplateLanguage();
		assertThat(language, equalTo(TemplateConstants.LANG_TYPE_SOY));
	}
	
	@Test
	public void activate_callsGetTemplateResource_withTheRightTemplateName() throws Exception {
		UserTextDDMFormFieldRenderer spiedUserTextDDMFormFieldRenderer = spy(userTextDDMFormFieldRenderer);
		doReturn(mockTemplateResource).when(spiedUserTextDDMFormFieldRenderer, "getTemplateResource", "/META-INF/resources/digitalplace-user-text-form-field-type.soy");
		
		spiedUserTextDDMFormFieldRenderer.activate(null);
		
		verifyPrivate(spiedUserTextDDMFormFieldRenderer, times(1)).invoke("getTemplateResource", "/META-INF/resources/digitalplace-user-text-form-field-type.soy");
	}
	
	@Test
	public void populateOptionalContext_WhenNoErrors_ThenTemplateContextContributorParameters() throws Exception {
		Map<String, Object> parameters = new HashMap<>();
		when(mockUserTextDDMFormFieldTemplateContextContributor.getParameters(mockDDMFormField, mockDDMFormFieldRenderingContext)).thenReturn(parameters);
		
		userTextDDMFormFieldRenderer.populateOptionalContext(mockTemplate, mockDDMFormField, mockDDMFormFieldRenderingContext);
		
		verify(mockTemplate, times(1)).putAll(parameters);
	}
}
