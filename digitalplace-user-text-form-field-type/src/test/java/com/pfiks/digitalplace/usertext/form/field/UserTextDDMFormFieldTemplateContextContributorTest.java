package com.pfiks.digitalplace.usertext.form.field;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.render.DDMFormFieldRenderingContext;
import com.liferay.expando.kernel.model.ExpandoBridge;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.security.auth.PrincipalThreadLocal;
import com.liferay.portal.kernel.service.UserLocalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest(PrincipalThreadLocal.class)
public class UserTextDDMFormFieldTemplateContextContributorTest {
	
	private final static String USER_FIELD_NAME = "userField";
	private final static long USER_ID = 23423L;

	private UserTextDDMFormFieldTemplateContextContributor userTextDDMFormFieldTemplateContextContributor;
	
	@Mock
	private DDMFormField mockDDMFormField;
	
	@Mock
	private DDMFormFieldRenderingContext mockDDMFormFieldRenderingContext;
	
	@Mock
	private UserLocalService mockUserLocalService;
	
	@Mock
	private User mockUser; 
	
	@Mock
	private ExpandoBridge mockExpandoBridge;
	
	@Before
	public void setUp() {
		userTextDDMFormFieldTemplateContextContributor = new UserTextDDMFormFieldTemplateContextContributor();
		userTextDDMFormFieldTemplateContextContributor.setUseLocalService(mockUserLocalService);
		
		mockStatic(PrincipalThreadLocal.class);
	}
	
	@Test
	public void getParameters_WhenUserFieldNameIsNull_ThenDoesNotSetPredefinedValueInParameters() {	
		
		when(mockDDMFormField.getProperty(UserTextDDMFormFieldTemplateContextContributor.USER_FIELD_NAME_PARAMETER)).thenReturn(null);
		when(mockDDMFormField.getProperty(UserTextDDMFormFieldTemplateContextContributor.EXPANDO_FIELD_PARAMETER)).thenReturn(false);
		
		Map<String, Object> parameters = userTextDDMFormFieldTemplateContextContributor.getParameters(mockDDMFormField, mockDDMFormFieldRenderingContext);
		
		assertThat(parameters.get("predefinedValue"), nullValue());
	}

	@Test
	public void getParameters_WhenUserIsNotLoggedIn_ThenDoesNotSetPredefinedValueInParameters() throws Exception {	
		
		when(mockDDMFormField.getProperty(UserTextDDMFormFieldTemplateContextContributor.USER_FIELD_NAME_PARAMETER)).thenReturn(USER_FIELD_NAME);
		when(mockDDMFormField.getProperty(UserTextDDMFormFieldTemplateContextContributor.EXPANDO_FIELD_PARAMETER)).thenReturn(false);
		when(PrincipalThreadLocal.getUserId()).thenReturn(USER_ID);
		when(mockUserLocalService.getUser(USER_ID)).thenThrow(new PortalException());
		
		Map<String, Object> parameters = userTextDDMFormFieldTemplateContextContributor.getParameters(mockDDMFormField, mockDDMFormFieldRenderingContext);
		
		assertThat(parameters.get("predefinedValue"), nullValue());
	}
	
	@Test
	public void getParameters_WhenExpandoFieldSettingIsTrue_AndExpandoFieldValueIsNotNull_ThenSetsExpandoValueAsPredefinedValueInParameters() throws Exception {	
		
		final String value = "value";
		
		when(mockDDMFormField.getProperty(UserTextDDMFormFieldTemplateContextContributor.USER_FIELD_NAME_PARAMETER)).thenReturn(USER_FIELD_NAME);
		when(mockDDMFormField.getProperty(UserTextDDMFormFieldTemplateContextContributor.EXPANDO_FIELD_PARAMETER)).thenReturn(true);
		when(PrincipalThreadLocal.getUserId()).thenReturn(USER_ID);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockUser.getExpandoBridge()).thenReturn(mockExpandoBridge);
		when(mockExpandoBridge.getAttribute(USER_FIELD_NAME)).thenReturn(value);
		
		Map<String, Object> parameters = userTextDDMFormFieldTemplateContextContributor.getParameters(mockDDMFormField, mockDDMFormFieldRenderingContext);
		
		assertThat(parameters.get("predefinedValue"), equalTo(value));
	}
	
	@Test
	public void getParameters_WhenExpandoFieldSettingIsTrue_AndExpandoFieldValueIsNull_ThenDoesNotSetPredefinedValueInParameters() throws Exception {	
		
		when(mockDDMFormField.getProperty(UserTextDDMFormFieldTemplateContextContributor.USER_FIELD_NAME_PARAMETER)).thenReturn(USER_FIELD_NAME);
		when(mockDDMFormField.getProperty(UserTextDDMFormFieldTemplateContextContributor.EXPANDO_FIELD_PARAMETER)).thenReturn(true);
		when(PrincipalThreadLocal.getUserId()).thenReturn(USER_ID);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockUser.getExpandoBridge()).thenReturn(mockExpandoBridge);
		when(mockExpandoBridge.getAttribute(USER_FIELD_NAME)).thenReturn(null);
		
		Map<String, Object> parameters = userTextDDMFormFieldTemplateContextContributor.getParameters(mockDDMFormField, mockDDMFormFieldRenderingContext);
		
		assertThat(parameters.get("predefinedValue"), nullValue());
	}
	
	@Test
	public void getParameters_WhenExpandoFieldeSettingIsFalse_AndGetterForUserFieldDoesNotExist_ThenDoesNotSetPredefinedValueInParameters() throws Exception {	
		
		when(mockDDMFormField.getProperty(UserTextDDMFormFieldTemplateContextContributor.USER_FIELD_NAME_PARAMETER)).thenReturn(USER_FIELD_NAME);
		when(mockDDMFormField.getProperty(UserTextDDMFormFieldTemplateContextContributor.EXPANDO_FIELD_PARAMETER)).thenReturn(false);
		
		Map<String, Object> parameters = userTextDDMFormFieldTemplateContextContributor.getParameters(mockDDMFormField, mockDDMFormFieldRenderingContext);
		
		assertThat(parameters.get("predefinedValue"), nullValue());
	}
	
	@Test
	public void getParameters_WhenExpandoFieldeSettingIsFalse_AndGetterForUserFieldReturnsNullObject_ThenDoesNotSetPredefinedValueInParameters() throws Exception {	
		when(PrincipalThreadLocal.getUserId()).thenReturn(USER_ID);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockUser.getFirstName()).thenReturn(null);
		
		when(mockDDMFormField.getProperty(UserTextDDMFormFieldTemplateContextContributor.USER_FIELD_NAME_PARAMETER)).thenReturn("firstName");
		when(mockDDMFormField.getProperty(UserTextDDMFormFieldTemplateContextContributor.EXPANDO_FIELD_PARAMETER)).thenReturn(false);
		
		Map<String, Object> parameters = userTextDDMFormFieldTemplateContextContributor.getParameters(mockDDMFormField, mockDDMFormFieldRenderingContext);
		
		assertThat(parameters.get("predefinedValue"), nullValue());
	}
	
	@Test
	public void getParameters_WhenExpandoFieldeSettingIsFalse_AndGetterForUserFieldReturnsNotNullObject_ThenSetsReturnedObjectToStringAsPredefinedValueInParameters() throws Exception {
		
		final String value = "firstNameValue";
		
		when(PrincipalThreadLocal.getUserId()).thenReturn(USER_ID);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockUser.getFirstName()).thenReturn(value);
		
		when(mockDDMFormField.getProperty(UserTextDDMFormFieldTemplateContextContributor.USER_FIELD_NAME_PARAMETER)).thenReturn("firstName");
		when(mockDDMFormField.getProperty(UserTextDDMFormFieldTemplateContextContributor.EXPANDO_FIELD_PARAMETER)).thenReturn(false);
		
		Map<String, Object> parameters = userTextDDMFormFieldTemplateContextContributor.getParameters(mockDDMFormField, mockDDMFormFieldRenderingContext);
		
		assertThat(parameters.get("predefinedValue"), equalTo(value));
	}

}
