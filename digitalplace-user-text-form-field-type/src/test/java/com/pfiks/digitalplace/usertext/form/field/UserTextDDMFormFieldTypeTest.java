package com.pfiks.digitalplace.usertext.form.field;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Before;
import org.junit.Test;


public class UserTextDDMFormFieldTypeTest {

	private UserTextDDMFormFieldType userTextDDMFormFieldType;
	
	@Before
	public void setUp() {
		userTextDDMFormFieldType = new UserTextDDMFormFieldType();
	}
	
	@Test
	public void getDDMFormFieldTypeSettings_whenNoErrors_returnsUserTextDDMFormFieldTypeSettingsClass() {
		Class typeSettingsClass = userTextDDMFormFieldType.getDDMFormFieldTypeSettings();
		assertThat(typeSettingsClass, equalTo(UserTextDDMFormFieldTypeSettings.class));
	}
	
	@Test
	public void getName_whenNoErrors_returnsUserTextFormFieldTypeName() {
		String name = userTextDDMFormFieldType.getName();
		assertThat(name, equalTo("digitalplaceUserTextFormFieldType"));
	}
}
